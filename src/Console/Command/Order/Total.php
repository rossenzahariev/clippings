<?php
namespace App\Console\Command\Order;

use App\Clippings\Import\Order;
use App\Clippings\Import\Order\Input;
use App\Console\Command\Command;
use App\Clippings\Import\Factory;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Total extends Command
{
	protected function configure()
	{
		$this->setName('app:order-total');
		$this->setDescription('Calculates total of the provided orders');

		$this->arguments = [
			'importFile',
			'currencyList',
			'outputCurrency'
		];
		$this->optional = [
			'customerVat',
		];

		parent::configure();
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try {
			$importInput = new Input(
				$input->getArgument('importFile'),
				$input->getArgument('currencyList'),
				$input->getArgument('outputCurrency'),
				(string)$input->getArgument('customerVat')
			);

			$orderImport = new Order();
			$orderImport->setInput($importInput);
			$orderImport->process();

			foreach ($orderImport->getTotals() as $total) {
				$output->writeln($total[0] . ' - ' . $total[1]);
			}
			return Command::SUCCESS;
		} catch (\Exception $e) {
			$output->writeln($e->getMessage());
			return Command::FAILURE;
		}
	}
}