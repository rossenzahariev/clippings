<?php
namespace App\Console\Command;

use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

abstract class Command extends SymfonyCommand
{
	/** @var  InputInterface */
	protected $input;
	/** @var  OutputInterface */
	protected $output;
	/** @var array */
	protected $arguments = [];
	/** @var array */
	protected $optional = [];

	protected function configure()
	{
		foreach ($this->arguments as $argument) {
			$this->addArgument($argument, InputArgument::REQUIRED);
		}
		foreach ($this->optional as $argument) {
            $this->addArgument($argument, InputArgument::OPTIONAL);
        }
	}
}