<?php
namespace App\Clippings\Import;

use App\Clippings\Import\Order\InputInterface;
use App\Clippings\Order\Document;
use App\Clippings\Order\CustomerCollection;
use App\Clippings\Service\Total;
use App\Clippings\Money\Converter;

class Order
{
	/** @var InputInterface */
	private $input;
	/** @var CustomerCollection */
	private $customerCollection;

	public function __construct()
	{
		$this->customerCollection = new CustomerCollection();
	}

	public function setInput(InputInterface $input): void
	{
		$this->input = $input;
	}

	public function getInput(): InputInterface
	{
		return $this->input;
	}

	public function process(): void
	{
		$this->createDocuments();
		$this->convertCurrency();
	}

	public function getTotals(): array
	{
		return $this->calculateCustomerTotals();
	}

	private function createDocuments(): void
	{
		foreach ($this->input->getData() as $dataRow) {
			$document = new Document();
			$document->setCustomer($dataRow[0]);
			$document->setVat($dataRow[1]);
			$document->setDocument($dataRow[2]);
			$document->setType($dataRow[3]);
			$document->setParent($dataRow[4]);
			$document->setCurrency($dataRow[5]);
			$document->setTotal($dataRow[6]);

			$this->customerCollection->add($document);
		}
	}

	private function convertCurrency(): void
	{
		$converter = new Converter();
		$converter->setCurrencies($this->input->getCurrencyList());
		$this->customerCollection->convert($converter, $this->input->getOutputCurrency());
	}

	private function calculateCustomerTotals(): array
	{
		$customerTotals = [];
		$customerVat = $this->input->getCustomerVat();
		$calculator = new Total();
		$collections = $customerVat ?
			[$customerVat => $this->customerCollection->getCustomerDocuments($customerVat)] :
			$this->customerCollection->toArray();

		foreach ($collections as $vat => $documentCollection) {
			$total = $calculator->calculate($documentCollection);
			$customerTotals[] = [$documentCollection->getCustomer(), $total->getAmount() . ' ' . $total->getCurrency()];
		}

		return $customerTotals;
	}
}