<?php
namespace App\Clippings\Import\Order;

use Money\Currency;

interface InputInterface
{
	public function getData(): array;

	public function getCurrencyList(): array;

	public function getOutputCurrency(): Currency;

	public function getCustomerVat(): string;
}