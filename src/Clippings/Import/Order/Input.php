<?php
namespace App\Clippings\Import\Order;

use Money\Currencies\ISOCurrencies;
use Money\Currency;

class Input implements InputInterface
{
	const FILE_UNREADABLE = 1001;
	const CURRENCY_LIST_FORMAT = 1002;
	const CURRENCY_UNKNOWN = 1003;
	const OUT_CURRENCY_UNKNOWN = 1004;

	/** @var array */
	private $importData = [];
	/** @var array */
	private $currencies = [];
	/** @var Currency */
	private $outputCurrency;
	/** @var string */
	private $customerVat;

	public function __construct(
		string $sourceFile,
		string $currencyList,
		string $outputCurrency,
		$customerVat = ''
	){
		$this->prepareFile($sourceFile);
		$this->prepareCurrencyList($currencyList);
		$this->prepareOutputCurrency($outputCurrency);
		$this->prepareCustomerVat($customerVat);
	}

	public function getData(): array
	{
		return $this->importData;
	}

	public function getCurrencyList(): array
	{
		return $this->currencies;
	}

	public function getOutputCurrency(): Currency
	{
		return $this->outputCurrency;
	}

	public function getCustomerVat(): string
	{
		return $this->customerVat;
	}

	protected function prepareFile(string $sourceFile): void
	{
		$sourceFile = getcwd() . DIRECTORY_SEPARATOR . $sourceFile;
		if (file_exists($sourceFile) === false || ($handle = fopen($sourceFile, 'r')) === false) {
			throw new \Exception(
				"The source file <$sourceFile> can not be accessed.",
				static::FILE_UNREADABLE
			);
		}

		$discardHeaderRow = fgetcsv($handle);
		while (($rowData = fgetcsv($handle)) !== false) {
			$this->importData[] = $rowData;
		}
		fclose($handle);
	}

	private function prepareCurrencyList(string $currencyList): void
	{
		$pattern = '/([a-zA-Z]+):([0-9]+\.?[0-9]*)/';
		$patternMatches = [];
		$knownCurrencies = new ISOCurrencies();

		$matchesCount = preg_match_all($pattern, $currencyList, $patternMatches);
		if (empty($patternMatches) || $matchesCount === 0) {
			throw new \Exception('Invalid currency list format', static::CURRENCY_LIST_FORMAT);
		}

		$matchedCurrencyCodes = $patternMatches[1];
		$exchangeRates = $patternMatches[2];
		foreach ($matchedCurrencyCodes as $currencyPosition => $currencyCode) {
			$currency = new Currency($currencyCode);
			$exchangeRate = floatval($exchangeRates[$currencyPosition]);

			if ($knownCurrencies->contains($currency) === false) {
				throw new \Exception("Unknown currency in the $currencyCode", Input::CURRENCY_UNKNOWN);
			}
			$this->currencies[] = [$currency, $exchangeRate];
		}
	}

	private function prepareOutputCurrency(string $outputCurrency): void
	{
		$currency = new Currency($outputCurrency);
		$knownCurrencies = new ISOCurrencies();
		if ($knownCurrencies->contains($currency) === false) {
			throw new \Exception("Unknown output in the $outputCurrency", Input::OUT_CURRENCY_UNKNOWN);
		}
		$this->outputCurrency = $currency;
	}

	private function prepareCustomerVat(string $customerVat)
	{
		$this->customerVat = $customerVat;
	}
}