<?php
namespace App\Clippings\Service;

use App\Clippings\Order\Document;
use App\Clippings\Order\DocumentCollection;
use App\Clippings\Service\Document\Sum;
use App\Clippings\Service\Document\TypeFactory;
use Money\Money;

class Total
{
	public function calculate(DocumentCollection $collection): Money
	{
		$total = null;
		foreach ($collection->toArray() as $record) {
			$total = $this->getTypeCalculator($record)->sum($total);
		}
		return $total;
	}

	protected function getTypeCalculator(Document $record): Sum
	{
		return TypeFactory::create($record);
	}
}