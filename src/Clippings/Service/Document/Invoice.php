<?php
namespace App\Clippings\Service\Document;

use App\Clippings\Order\Document;
use Money\Money;

class Invoice implements Sum
{
	/** @var Document */
	protected $document;

	public function __construct(Document $document)
	{
		$this->document = $document;
	}

	public function sum($total = null): Money
	{
		$documentTotal = $this->document->getTotal();
		return $total ? $total->add($documentTotal) : $documentTotal;
	}
}