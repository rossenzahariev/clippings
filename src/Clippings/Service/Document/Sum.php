<?php
namespace App\Clippings\Service\Document;

use Money\Money;

interface Sum
{
	/**
	 * @param Money|null $total
	 * @return Money
	 */
	public function sum(Money $total): Money;
}