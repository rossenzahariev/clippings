<?php
namespace App\Clippings\Service\Document;

use App\Clippings\Order\Document;

class TypeFactory
{
	public static function create(Document $document): Sum
	{
		switch ($document->getType()) {
			case 1:
				return new Invoice($document);
				break;
			case 2:
				return new Credit($document);
				break;
			case 3:
				return new Debit($document);
				break;
			default:
				throw new \Exception('Invalid record type');
				break;
		}
	}
}