<?php
namespace App\Clippings\Service\Document;

use Money\Money;

class Credit extends Invoice
{
	public function sum($total = null): Money
	{
		$documentTotal = $this->document->getTotal();
		return $total ? $total->subtract($documentTotal) : $documentTotal->multiply(-1);
	}
}