<?php
namespace App\Clippings\Money;

use App\Clippings\Order\Document;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Exchange\FixedExchange;
use Money\Exchange\ReversedCurrenciesExchange;
use Money\Converter as MoneyConverter;

class Converter
{
	/** @var Currency */
	private $baseCurrency;
	/** @var MoneyConverter[]  */
	private $exchanges = [];
	/** @var MoneyConverter[] */
	private $reverseExchanges = [];

	/**
	 * The expected format is for example
	 * [
	 * 		[Currency $instance, float $exchangeRate],
	 * 		[Currency $instance, float $exchangeRate],
	 * ]
	 * @param array $availableCurrencies
	 */
	public function setCurrencies(array $availableCurrencies): void
	{
		$this->determineDefaultCurrency($availableCurrencies);
		$this->buildExchanges($availableCurrencies);
	}

	private function determineDefaultCurrency($availableCurrencies): void
	{
		foreach ($availableCurrencies as $currencyEntry) {
			if ($currencyEntry[1] === 1.0) {
				$this->baseCurrency = $currencyEntry[0];
				break;
			}
		}
	}

	private function buildExchanges($availableCurrencies): void
	{
		foreach ($availableCurrencies as $currencyEntry) {
			$currency = $currencyEntry[0];
			$exchangeRate = $currencyEntry[1];
			if ($this->baseCurrency->equals($currency)) {
				continue;
			}

			$exchange = new FixedExchange([
				$this->baseCurrency->getCode() => [
					$currency->getCode() => $exchangeRate
				]
			]);
			$reverseExchange = new ReversedCurrenciesExchange($exchange);

			$this->exchanges[$currency->getCode()] = new MoneyConverter(new ISOCurrencies(), $exchange);
			$this->reverseExchanges[$currency->getCode()] = new MoneyConverter(new ISOCurrencies(), $reverseExchange);
		}
	}

	public function toBaseCurrency(Document $document): void
	{
		if ($document->getCurrency()->equals($this->baseCurrency)) {
			return;
		}
		$converter = $this->getReverseExchange($document->getCurrency());
		$convertedTotal = $converter->convert($document->getTotal(), $this->baseCurrency);
		$document->setCurrency($this->baseCurrency);
		$document->setTotal($convertedTotal);
	}

	public function toCurrency(Document $document, Currency $currency): void
	{
		if ($document->getCurrency()->equals($currency)) {
			return;
		}

		$this->toBaseCurrency($document);
		if ($currency->equals($this->baseCurrency)) {
			return;
		}
		$converter = $this->getExchange($currency);
		$convertedTotal = $converter->convert($document->getTotal(), $currency);
		$document->setCurrency($currency);
		$document->setTotal($convertedTotal);
	}

	private function getReverseExchange(Currency $currency): MoneyConverter
	{
		if (empty($this->reverseExchanges[$currency->getCode()])) {
			throw new \Exception('Undefined exchange rate for: ' . $currency->getCode(), 2001);
		}
		return $this->reverseExchanges[$currency->getCode()];
	}

	private function getExchange(Currency $currency): MoneyConverter
	{
		if (empty($this->exchanges[$currency->getCode()])) {
			throw new \Exception('Undefined exchange rate for: ' . $currency->getCode());
		}
		return $this->exchanges[$currency->getCode()];
	}

}