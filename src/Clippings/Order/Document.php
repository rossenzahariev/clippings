<?php
namespace App\Clippings\Order;

use Money\Currency;
use Money\Money;

class Document
{
	/** @var string */
	private $customer;
	/** @var string */
	private $vat;
	/** @var string */
	private $document;
	/** @var string */
	private $type;
	/** @var string */
	private $parent;
	/** @var array */
	private $children;
	/** @var Currency */
	private $currency;
	/** @var Money */
	private $total;

	/**
	 * @return string
	 */
	public function getCustomer(): string
	{
		return $this->customer;
	}

	/**
	 * @param string $customer
	 */
	public function setCustomer(string $customer)
	{
		$this->customer = $customer;
	}

	/**
	 * @return string
	 */
	public function getVat(): string
	{
		return $this->vat;
	}

	/**
	 * @param string $vat
	 */
	public function setVat(string $vat)
	{
		$this->vat = $vat;
	}

	/**
	 * @return string
	 */
	public function getDocument(): string
	{
		return $this->document;
	}

	/**
	 * @param string $document
	 */
	public function setDocument(string $document)
	{
		$this->document = $document;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type)
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getParent(): string
	{
		return $this->parent;
	}

	/**
	 * @param string $parent
	 */
	public function setParent(string $parent)
	{
		$this->parent = $parent;
	}

	/**
	 * @return Currency
	 */
	public function getCurrency(): Currency
	{
		return $this->currency;
	}

	/**
	 * @param Currency|string $currency
	 */
	public function setCurrency($currency)
	{
		if (is_string($currency)) {
			$currency = new Currency($currency);
		}
		$this->currency = $currency;
	}

	/**
	 * @return Money
	 */
	public function getTotal(): Money
	{
		return $this->total;
	}

	/**
	 * @param Money|string|int $total
	 * @throws \Exception
	 */
	public function setTotal($total)
	{
		if (is_string($total) || is_int($total)) {
			$total = new Money($total, $this->currency);
		}

		if ($this->currency->getCode() !== $total->getCurrency()->getCode()) {
			throw new \Exception('Mismatch between document currency and total currency');
		}

		$this->total = $total;
	}

	public function addChild(Document $record)
	{
		if (isset($this->children[$record->getDocument()])) {
			return;
		}
		$this->children[$record->getDocument()] = $record;
	}

	public function getChildren(): array
	{
		return $this->children;
	}
}