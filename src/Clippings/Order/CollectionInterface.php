<?php
namespace App\Clippings\Order;

use App\Clippings\Money\Converter;
use Money\Currency;

interface CollectionInterface
{
	public function add(Document $document): void;

	public function toArray(): array;

	public function getDocuments(): array;

	public function getCustomerDocuments(string $vat): DocumentCollection;

	public function getCustomer(string $vat = ''): string;

	public function convert(Converter $converter, Currency $toCurrency): void;
}