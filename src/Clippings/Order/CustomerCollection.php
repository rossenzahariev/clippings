<?php
namespace App\Clippings\Order;

use App\Clippings\Money\Converter;
use Money\Currency;

class CustomerCollection implements CollectionInterface
{
	/** @var DocumentCollection[]  */
	private $customers = [];

	public function add(Document $document): void
	{
		if (!isset($this->customers[$document->getVat()])) {
			$this->customers[$document->getVat()] = new DocumentCollection();
		}
		$this->customers[$document->getVat()]->add($document);
	}

	public function toArray(): array
	{
		return $this->customers;
	}

	public function getDocuments(): array
	{
		$documents = [];
		foreach ($this->customers as $documentCollection) {
			$documents = array_merge($documents, $documentCollection->toArray());
		}
		return $documents;
	}

	public function getCustomerDocuments(string $vat): DocumentCollection
	{
		return $this->customers[$vat];
	}

	public function getCustomer(string $vat = ''): string
	{
		$customer = '';
		if (empty($vat)) {
			$customer = current($this->customers)->getCustomer();
		}
		foreach ($this->customers as $customerCollection) {
			if ($customer = $customerCollection->getCustomer($vat)) {
				break;
			}
		}
		return $customer;
	}

	public function convert(Converter $converter, Currency $toCurrency): void
	{
		foreach ($this->customers as $customer => $documentCollection) {
			$this->customers[$customer]->convert($converter, $toCurrency);
		}
	}
}