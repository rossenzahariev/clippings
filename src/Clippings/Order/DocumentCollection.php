<?php
namespace App\Clippings\Order;

use App\Clippings\Money\Converter;
use Money\Currency;

class DocumentCollection implements CollectionInterface
{
	/** @var Document[]  */
	private $documents = [];

	public function add(Document $document): void
	{
		$this->documents[$document->getDocument()] = $document;
		if ($document->getParent()) {
			if (isset($this->documents[$document->getParent()]) === false) {
				throw new \Exception('Unknown parent document for document #' . $document->getDocument());
			}
			$parent = $this->documents[$document->getParent()];
			$parent->addChild($document);
		}
	}

	public function toArray(): array
	{
		return $this->documents;
	}

	public function getDocuments(): array
	{
		return $this->toArray();
	}

	public function getCustomerDocuments(string $vat): DocumentCollection
	{
		$customerDocuments = new DocumentCollection();
		foreach ($this->documents as $document) {
			if ($document->getVat() === $vat) {
				$customerDocuments->add($document);
			}
		}
		return $customerDocuments;
	}

	public function getCustomer(string $vat = ''): string
	{
		$customer = '';
		if (empty($vat)) {
			$customer = current($this->documents)->getCustomer();
		} else {
			foreach ($this->documents as $document) {
				if ($document->getVat() === $vat) {
					$customer = $document->getCustomer();
					break;
				}
			}
		}
		return $customer;
	}

	public function convert(Converter $converter, Currency $toCurrency): void
	{
		foreach ($this->documents as $document) {
			$converter->toCurrency($document, $toCurrency);
		}
	}
}