<?php
namespace App\Tests\Clippings\Import;

use App\Clippings\Import\Order;
use App\Clippings\Import\Order\Input;
use Money\Currency;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
	private $fixtures = [
		'unsupportedCurrencyInDataSet' => [
			'data' => [
				['vendor 1', '7890', '1000', '1', '', 'USD', '100'],
				['vendor 1', '7890', '1001', '1', '', 'BGL', '100'],
			],
			'currencyList' => [
				['EUR', 1.0],
				['USD', 2.0]
			],
			'outputCurrency' => 'USD',
			'customerVat' => ''
		],

		'unsupportedOutputCurrency' => [
			'data' => [
				['vendor 1', '7890', '1000', '1', '', 'USD', '100'],
				['vendor 1', '7890', '1001', '1', '', 'EUR', '100'],
			],
			'currencyList' => [
				['EUR', 1.0],
				['USD', 2.0]
			],
			'outputCurrency' => 'GBP',
			'customerVat' => ''
		],

		'simpleCalculation' => [
			'data' => [
				['vendor 1', '7890', '1000', '1', '', 'EUR', '100'],
				['vendor 1', '7890', '1001', '1', '', 'USD', '100'],
			],
			'currencyList' => [
				['EUR', 1.0],
				['USD', 2.0]
			],
			'outputCurrency' => 'EUR',
			'customerVat' => '',
			'expected' => [
				['vendor 1', '150 EUR']
			]
		],

		'negativeCalculation' => [
			'data' => [
				['vendor 1', '7890', '1000', '1', '', 'EUR', '100'],
				['vendor 1', '7890', '1001', '2', '', 'USD', '400'],
			],
			'currencyList' => [
				['EUR', 1.0],
				['USD', 2.0]
			],
			'outputCurrency' => 'EUR',
			'customerVat' => '',
			'expected' => [
				['vendor 1', '-100 EUR']
			]
		],

		'multipleVendorsCalculation' => [
			'data' => [
				['vendor 1', '7890', '1000', '1', '', 'EUR', '100'],
				['vendor 1', '7890', '1001', '2', '', 'USD', '400'],
				['vendor 2', '1234', '2000', '1', '', 'EUR', '100'],
				['vendor 2', '1234', '2001', '3', '', 'USD', '100'],
			],
			'currencyList' => [
				['EUR', 1.0],
				['USD', 2.0]
			],
			'outputCurrency' => 'EUR',
			'customerVat' => '',
			'expected' => [
				['vendor 1', '-100 EUR'],
				['vendor 2', '150 EUR']
			]
		],

		'multipleVendorsCalculationPickOne' => [
			'data' => [
				['vendor 1', '7890', '1000', '1', '', 'EUR', '100'],
				['vendor 1', '7890', '1001', '2', '', 'USD', '400'],
				['vendor 2', '1234', '2000', '1', '', 'EUR', '100'],
				['vendor 2', '1234', '2001', '1', '', 'USD', '100'],
			],
			'currencyList' => [
				['EUR', 1.0],
				['USD', 2.0]
			],
			'outputCurrency' => 'EUR',
			'customerVat' => '1234',
			'expected' => [
				['vendor 2', '150 EUR']
			]
		],
	];

	public function testSetInput()
	{
		$inputBuilder = $this->getInputMockBuilder();
		$inputMock = $inputBuilder->getMock();

		$sut = new Order();
		$sut->setInput($inputMock);

		$this->assertSame($inputMock, $sut->getInput());
	}

	public function testUnsupportedCurrencyInDataSet()
	{
		$inputBuilder = $this->getInputMockBuilder();
		$inputMock = $inputBuilder->getMock();
		$this->setupInputMockFixture($inputMock, $this->fixtures['unsupportedCurrencyInDataSet']);

		$this->expectExceptionMessage('Undefined exchange rate for: BGL');

		$sut = new Order();
		$sut->setInput($inputMock);
		$sut->process();
	}

	public function testUnsupportedOutputCurrencyRate()
	{
		$inputBuilder = $this->getInputMockBuilder();
		$inputMock = $inputBuilder->getMock();
		$this->setupInputMockFixture($inputMock, $this->fixtures['unsupportedOutputCurrency']);

		$this->expectExceptionMessage('Undefined exchange rate for: GBP');

		$sut = new Order();
		$sut->setInput($inputMock);
		$sut->process();
	}

	public function testSimpleCalculation()
	{
		$fixture = $this->fixtures['simpleCalculation'];
		$inputBuilder = $this->getInputMockBuilder();
		$inputMock = $inputBuilder->getMock();
		$this->setupInputMockFixture($inputMock, $fixture);

		$sut = new Order();
		$sut->setInput($inputMock);
		$sut->process();
		$totals = $sut->getTotals();

		$this->assertExpectations($fixture['expected'], $totals);
	}

	public function testNegativeCalculation()
	{
		$fixture = $this->fixtures['negativeCalculation'];
		$inputBuilder = $this->getInputMockBuilder();
		$inputMock = $inputBuilder->getMock();
		$this->setupInputMockFixture($inputMock, $fixture);

		$sut = new Order();
		$sut->setInput($inputMock);
		$sut->process();
		$totals = $sut->getTotals();

		$this->assertExpectations($fixture['expected'], $totals);
	}

	public function testMultipleVendors()
	{
		$fixture = $this->fixtures['multipleVendorsCalculation'];
		$inputBuilder = $this->getInputMockBuilder();
		$inputMock = $inputBuilder->getMock();
		$this->setupInputMockFixture($inputMock, $fixture);

		$sut = new Order();
		$sut->setInput($inputMock);
		$sut->process();
		$totals = $sut->getTotals();

		$this->assertExpectations($fixture['expected'], $totals);
	}

	public function testMultipleVendorsPickOne()
	{
		$fixture = $this->fixtures['multipleVendorsCalculationPickOne'];
		$inputBuilder = $this->getInputMockBuilder();
		$inputMock = $inputBuilder->getMock();
		$this->setupInputMockFixture($inputMock, $fixture);

		$sut = new Order();
		$sut->setInput($inputMock);
		$sut->process();
		$totals = $sut->getTotals();

		$this->assertExpectations($fixture['expected'], $totals);
	}

	private function getInputMockBuilder(): MockBuilder
	{
		$mockBuilder = $this->getMockBuilder(Input::class);
		$mockBuilder->disableOriginalConstructor();
		$mockBuilder->setMethods(['getData', 'getCurrencyList', 'getOutputCurrency', 'getCustomerVat']);
		return $mockBuilder;
	}

	private function setupInputMockFixture($inputMock, $fixture)
	{
		$inputMock->expects($this->any())
			->method('getData')
			->will($this->returnValue($fixture['data']));
		foreach ($fixture['currencyList'] as $key => $currencyRate) {
			$fixture['currencyList'][$key] = [new Currency($currencyRate[0]), $currencyRate[1]];
		}
		$inputMock->expects($this->any())
			->method('getCurrencyList')
			->will($this->returnValue($fixture['currencyList']));
		$inputMock->expects($this->any())
			->method('getOutputCurrency')
			->will($this->returnValue(new Currency($fixture['outputCurrency'])));
		$inputMock->expects($this->any())
			->method('getCustomerVat')
			->will($this->returnValue($fixture['customerVat']));
	}

	private function assertExpectations($expectations, $totals)
	{
		$this->assertEquals(count($expectations), count($totals));
		foreach ($expectations as $position => $expectation) {
			$this->assertEquals($expectation[0], $totals[$position][0]);
			$this->assertEquals($expectation[1], $totals[$position][1]);
		}
	}
}