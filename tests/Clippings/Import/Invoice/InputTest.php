<?php
namespace App\Tests\Clippings\Import;

use App\Clippings\Import\Order\Input;
use PHPUnit\Framework\TestCase;

class InputTest extends TestCase
{
	const FIXTURE_FILE = 'import/data.csv';
	const BAD_FIXTURE_FILE = 'import/badData.csv';

	public function testInvalidFile()
	{
		$this->expectExceptionCode(Input::FILE_UNREADABLE);

		new Input('fileNotFound.txt', '', '', '');
	}

	public function testInvalidCurrencyList()
	{
		$this->expectExceptionCode(Input::CURRENCY_LIST_FORMAT);

		new Input(static::FIXTURE_FILE, 'EUR-1,USD:,GBP:rate', '', '');
	}

	public function testUnknownCurrencyList()
	{
		$this->expectExceptionCode(Input::CURRENCY_UNKNOWN);

		new Input(static::FIXTURE_FILE, 'EUR:1,FOOBAR:0.987,GBP:0.878', '', '');
	}

	public function testInvalidOutputCurrencyList()
	{
		$this->expectExceptionCode(Input::OUT_CURRENCY_UNKNOWN);

		new Input(static::FIXTURE_FILE, 'EUR:1,USD:0.987,GBP:0.878', 'BAR', '');
	}

	public function testValidInput()
	{
		$input = new Input(static::FIXTURE_FILE, 'EUR:1,USD:0.987,GBP:0.878', 'USD', '123456');
		$this->assertNotEmpty($input->getData());
		$this->assertNotEmpty($input->getCurrencyList());
		$this->assertEquals('USD', $input->getOutputCurrency()->getCode());
		$this->assertEquals('123456', $input->getCustomerVat());
	}

}