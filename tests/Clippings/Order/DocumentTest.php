<?php
namespace App\Tests\Clippings\Order;

use App\Clippings\Order\Document;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;

class DocumentTest extends TestCase
{
	public function testAccessors()
	{
		$document = new Document();
		$parentDocument = new Document();

		$document->setCustomer('customer name');
		$this->assertEquals('customer name', $document->getCustomer());

		$document->setVat('1111');
		$this->assertEquals('1111', $document->getVat());

		$document->setDocument('2222');
		$this->assertEquals('2222', $document->getDocument());

		$document->setCurrency(new Currency('EUR'));
		$this->assertEquals(new Currency('EUR'), $document->getCurrency());
		$document->setCurrency('EUR');
		$this->assertEquals(new Currency('EUR'), $document->getCurrency());

		$document->setTotal(new Money('100', new Currency('EUR')));
		$this->assertEquals(new Money('100', new Currency('EUR')), $document->getTotal());
		$document->setTotal(100);
		$this->assertEquals(new Money('100', new Currency('EUR')), $document->getTotal());
		$document->setTotal('100');
		$this->assertEquals(new Money('100', new Currency('EUR')), $document->getTotal());

		$parentDocument->addChild($document);
		$this->assertEquals([$document->getDocument() => $document], $parentDocument->getChildren());

		$this->expectException(\Exception::class);
		$document->setTotal(new Money('100', new Currency('BGL')));
	}
}