<?php
namespace App\Tests\Clippings\Money;

use App\Clippings\Money\Converter;
use App\Clippings\Order\Document;
use Money\Currency;
use Money\Money;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\TestCase;

class ConverterTest extends TestCase
{
	public function testUnsupportedCurrency()
	{
		$documentBuilder = $this->getDocumentMockBuilder();
		$document = $documentBuilder->getMock();
		$document->expects($this->any())
			->method('getCurrency')
			->will($this->returnValue(new Currency('USD')));
		$document->expects($this->any())
			->method('getTotal')
			->will($this->returnValue(new Money('200', new Currency('USD'))));

		$sut = new Converter();
		$sut->setCurrencies([
			[new Currency('EUR'), 1.0],
			[new Currency('USD'), 2.0],
		]);

		$this->expectExceptionMessage('Undefined exchange rate for: BGL');
		$sut->toCurrency($document, new Currency('BGL'));
	}

	public function testConversions()
	{
		$document = new Document();
		$document->setCurrency(new Currency('USD'));
		$document->setTotal(new Money('200', new Currency('USD')));

		$sut = new Converter();
		$sut->setCurrencies([
			[new Currency('EUR'), 1.0],
			[new Currency('USD'), 2.0],
			[new Currency('GBP'), 0.5],
		]);

		$sut->toCurrency($document, new Currency('GBP'));
		$this->assertEquals(new Currency('GBP'), $document->getCurrency());
		$this->assertEquals(new Money('50', new Currency('GBP')), $document->getTotal());

		$sut->toBaseCurrency($document);
		$this->assertEquals(new Currency('EUR'), $document->getCurrency());
		$this->assertEquals(new Money('100', new Currency('EUR')), $document->getTotal());
	}

	private function getDocumentMockBuilder(): MockBuilder
	{
		$mockBuilder = $this->getMockBuilder(Document::class);
		$mockBuilder->disableOriginalConstructor();
		$mockBuilder->setMethods(['getCurrency', 'getTotal', 'setCurrency', 'setTotal']);
		return $mockBuilder;
	}
}